﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VanidaySPA.Helpers
{
    public static partial class APIHelper
    {
        public static string _apiBaseURI = "http://vaniday.api/api";
        public static string GetRoomTypesUrl = "/roomtype/getroomtypes";
        public static string GetRoomStatusesUrl = "/roomstatus/getroomstatuses";
        public static string GetRoomBookingsUrl = "/roombooking/getall";
        public static string SaveBulkBookingUrl = "/roombooking/create";
        public static string UpdateRoomRateUrl = "/roombooking/updateroomrate";
    }
}
