﻿angular.module('vanidayAppHelper', [])
    .factory("roomsServicesFactory", ['$http','$q', function ($http,$q) {

        var roomServicesFactory = {};

        roomServicesFactory.GetRoomTypes = function () {

            return $http.get("/roomtype/getroomtypes");

        };

        roomServicesFactory.GetRoomStatuses = function () {

            return $http.get("/roomstatus/getroomstatuses");

        };

        roomServicesFactory.GetRoomBookings = function () {
            
            return $http.get("/bookingmanager/getroombookings");
            
        };

        roomServicesFactory.SaveBulkBooking = function (booking) {
            var deferred = $q.defer();
            var data = {
                RoomTypeId: booking.RoomType,
                RoomStatusId: booking.RoomStatus,
                BookedFrom: booking.BookedFrom,
                BookedTo: booking.BookedTo,
                BookingRate: booking.BookingRate
            };
            
            var config = { headers: { 'Content-Type': 'application/json' } };
            
            $http.post('/bookingmanager/savebulkbooking', data, config)
                .then(
                function (response) {
                    console.log('success : ', response);
                    deferred.resolve('successfully saved');
                },
                function (response) {
                    console.log('error : ', response);
                    deferred.reject('error saving');
                }
            );
            return deferred.promise;
        };

        roomServicesFactory.UpdateRoomRate = function (booking) {
            var deferred = $q.defer();
            var data = {
                Id: booking.Id,
                BookingRate: booking.BookingRate
            };

            var config = { headers: { 'Content-Type': 'application/json' } };

            $http.post('/bookingmanager/updateroomrate', data, config)
                .then(
                function (response) {
                    console.log('success : ', response);
                    deferred.resolve('successfully updated');
                },
                function (response) {
                    console.log('error : ', response);
                    deferred.reject('error saving');
                }
            );
            return deferred.promise;
        };

        roomServicesFactory.DatePickerConfig = function () {
            var datePickerConfig = {};

            datePickerConfig.startDateStatus = false;
            datePickerConfig.endDateStatus = false;
            datePickerConfig.fromDate = new Date();
            datePickerConfig.toDate = new Date();

            datePickerConfig.options = {
                minDate: new Date(),
            };
            datePickerConfig.options2 = {
                minDate: new Date(datePickerConfig.fromDate),
            };
            datePickerConfig.toggleStartDate = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                datePickerConfig.startDateStatus = true;
            };

            //toggle end date popup
            datePickerConfig.toggleEndDate = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                datePickerConfig.endDateStatus = true;
            };

            datePickerConfig.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            datePickerConfig.format = datePickerConfig.formats[0];
            return datePickerConfig;
        };

        return roomServicesFactory;
    }]);

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode !== 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;
    return true;
}  