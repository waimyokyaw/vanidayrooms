﻿angular.module('vanidayApp', ['ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ui.calendar', 'vanidayAppHelper'])
    .controller('VanidayController', function ($scope, $http, roomsServicesFactory) {

        //Datepicker Plugins Config
        $scope.DatePickerConfig = roomsServicesFactory.DatePickerConfig();
        //Prompt Roomrate input
        $scope.promptPriceRateInput = function (date, jsEvent, view) {

            var rate = prompt(date.title);
            var data = {
                Id: date.id,
                BookingRate: rate
            };
            var updateRoom = roomsServicesFactory.UpdateRoomRate(data);
            updateRoom.then(function () {
                $scope.getRoomBookings();
            }, function (reason) {
                alert('Failed: ' + reason);
            });
            
            
        };

        //Calendar
        $scope.uiConfig = {
            calendar: {
                height: 400,
                editable: true,
                header: {
                    left: 'month,basicWeek,basicDay',
                    center: 'title',
                    right: 'prev,next today'
                },
                displayEventTime: false,
                eventClick: $scope.promptPriceRateInput,
                
                selectable: true,
                selectHelper: true,
                unselectAuto: true
            }
        };
        

        $scope.eventSources = [];
        $scope.LoadBookingsCalendar = function (bookingData) {

            var calEventsExt = {
                id: 2,
                visible: true,
                color: 'green',
                textColor: '#fff',
                className: ['fc-id-2'],
                events: []
            };

            $scope.bookingCalEvents = [];
            angular.forEach(bookingData, function (value, key) {

                var eventObj = {
                    id: value.id,
                    type: 'booking',
                    title: value.bookingRate,
                    start: value.bookedFrom,
                    end: value.bookedTo,
                    roomtypeid: value.roomTypeId
                };

                $scope.bookingCalEvents.push(eventObj);
            });
            angular.element('bookingCalendar').fullCalendar('option', 'timezone', 'local'); //set timezone to local as dates are being saved on server in utc.
            calEventsExt.events = angular.copy($scope.bookingCalEvents);

            $scope.eventSources.push(calEventsExt);           
        };

        //App data service
        $scope.BulkBooking = {};
        $scope.calenderRoomType = {};
        $scope.roomTypes = [];
        $scope.roomStatuses = [];
        $scope.roomBookings = [];
        $scope.getRoomTypes = function () {
            var roomTypes = roomsServicesFactory.GetRoomTypes();

            roomTypes.then(function (response) {
                $scope.roomTypes = response.data;
            }, function (error) {
                $scope.status = 'Unable to load room type data: ' + error.message;
            });
        };
        $scope.getRoomTypes();
        
        $scope.getRoomStatuses = function () {
            var roomStatuses = roomsServicesFactory.GetRoomStatuses();

            roomStatuses.then(function (response) {
                $scope.roomStatuses = response.data;
            }, function (error) {
                $scope.status = 'Unable to load room type data: ' + error.message;
            });
        };
        $scope.getRoomStatuses();

        $scope.SaveBulkBooking = function () {
            var saveBulkBookings = roomsServicesFactory.SaveBulkBooking($scope.BulkBooking);
            saveBulkBookings.then(function () {
                $scope.getRoomBookings();
            }, function (reason) {
                alert('Failed: ' + reason);
            });
            
        };

        $scope.getRoomBookings = function () {
            $scope.eventSources.splice(0);
            var roomBookings = roomsServicesFactory.GetRoomBookings();
            roomBookings.then(function (response) {

                $scope.roomBookings = response.data;
                $scope.LoadBookingsCalendar(response.data);
            }, function (error) {
                $scope.status = 'Unable to load room type data: ' + error.message;
            });
        };
        $scope.getRoomBookings();

        $scope.SelectRoomTypeCalendar = function () {
            $scope.eventSources.splice(0);
            var rtId = $scope.calenderRoomType;
            var rtBookings = [];
            angular.forEach($scope.roomBookings, function (value, key) {

                if (value.roomTypeId === rtId) {
                    rtBookings.push(value);
                }

            });

            $scope.LoadBookingsCalendar(rtBookings);
        };
    });