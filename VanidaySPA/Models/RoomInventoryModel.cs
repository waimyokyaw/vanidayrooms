﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VanidaySPA.Models
{
    public class RoomInventoryModel
    {
        public Guid Id { get; set; }
        public RoomTypeModel RoomType { get; set; }
        public Int32 NumberOfVacant { get; set; }
        public decimal RoomRate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
