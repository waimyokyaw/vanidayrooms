﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VanidaySPA.Models
{
    public class RoomBookingModel
    {
        public Guid Id { get; set; }
        public Guid RoomTypeId { get; set; }
        public Guid RoomStatusId { get; set; }
        public decimal BookingRate { get; set; }
        public DateTime BookedFrom { get; set; }
        public DateTime BookedTo { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
