﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VanidaySPA.Models
{
    public class RoomTypeModel
    {
        public Guid Id { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomTypeDescription { get; set; }
    }
}
