﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VanidaySPA.Helpers;
using VanidaySPA.Models;
using System.Net.Http;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VanidaySPA.Controllers
{
    public class RoomStatusController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetRoomStatuses()
        {
            List<RoomStatusModel> roomStatuses = new List<RoomStatusModel>();
            HttpClient client = APIHelper.InitializeClient();
            Console.WriteLine(client.BaseAddress + APIHelper.GetRoomStatusesUrl);
            
            HttpResponseMessage res = await client.GetAsync(client.BaseAddress + APIHelper.GetRoomStatusesUrl);
            Console.WriteLine("CALLING DONE " + res.StatusCode);
            //Checking the response is successful or not which is sent using HttpClient  
            if (res.IsSuccessStatusCode)
            {
                //Storing the response details recieved from web api   
                var result = res.Content.ReadAsStringAsync().Result;

                //Deserializing the response recieved from web api and storing into the Employee list  
                roomStatuses = JsonConvert.DeserializeObject<List<RoomStatusModel>>(result);

            }

            return new JsonResult(roomStatuses);
        }
        
    }
}
