﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VanidaySPA.Helpers;
using VanidaySPA.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace VanidaySPA.Controllers
{
    public class RoomTypeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetRoomTypes()
        {
            List<RoomTypeModel> roomTypes = new List<RoomTypeModel>();
            HttpClient client = APIHelper.InitializeClient();

            HttpResponseMessage res = await client.GetAsync(client.BaseAddress + APIHelper.GetRoomTypesUrl);
            
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                
                roomTypes = JsonConvert.DeserializeObject<List<RoomTypeModel>>(result);
            }

            return new JsonResult(roomTypes);
        }


    }
}
