﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VanidaySPA.Models;
using System.Net.Http;
using VanidaySPA.Helpers;
using Newtonsoft.Json;
using System.Text;

namespace VanidaySPA.Controllers
{
    public class BookingManagerController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<JsonResult> SaveBulkBooking([FromBody]RoomBookingModel roomBooking)
        {
            string result = string.Empty;
            var httpContentRoomBookingModel = new StringContent(JsonConvert.SerializeObject(roomBooking), Encoding.UTF8, "application/json");

            HttpClient client = APIHelper.InitializeClient();

            HttpResponseMessage res = await client.PostAsync(client.BaseAddress + APIHelper.SaveBulkBookingUrl, httpContentRoomBookingModel);

            if (res.IsSuccessStatusCode)
            {
                result = res.StatusCode + "Success";
            }
            else
            {
                result = res.StatusCode.ToString() + " | " + res.ReasonPhrase;
            }

            return new JsonResult(result);
        }

        public async Task<JsonResult> GetRoomBookings()
        {
            List<RoomBookingModel> roomBookings = new List<RoomBookingModel>();
            HttpClient client = APIHelper.InitializeClient();

            HttpResponseMessage res = await client.GetAsync(client.BaseAddress + APIHelper.GetRoomBookingsUrl);

            //Checking the response is successful or not which is sent using HttpClient  
            if (res.IsSuccessStatusCode)
            {
                //Storing the response details recieved from web api   
                var result = res.Content.ReadAsStringAsync().Result;

                //Deserializing the response recieved from web api and storing into the Employee list  
                roomBookings = JsonConvert.DeserializeObject<List<RoomBookingModel>>(result);

            }

            return new JsonResult(roomBookings);
        }

        public async Task<JsonResult> UpdateRoomRate([FromBody]RoomBookingModel roomBooking)
        {
            string result = string.Empty;
            var httpContentRoomBookingModel = new StringContent(JsonConvert.SerializeObject(roomBooking), Encoding.UTF8, "application/json");

            HttpClient client = APIHelper.InitializeClient();

            HttpResponseMessage res = await client.PostAsync(client.BaseAddress + APIHelper.UpdateRoomRateUrl, httpContentRoomBookingModel);

            if (res.IsSuccessStatusCode)
            {
                result = res.StatusCode + "Success";
            }
            else
            {
                result = res.StatusCode.ToString() + " | " + res.ReasonPhrase;
            }

            return new JsonResult(result);
        }
    }
}
