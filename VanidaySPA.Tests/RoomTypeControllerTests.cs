﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Vaniday.API.Controllers;
using Vaniday.API.Models;
using Vaniday.API.Models.Repository;
using System;

namespace VanidaySPA.Tests
{
    public class RoomTypeControllerTests
    {
        [Fact]
        public async Task TestGetRoomTypesList()
        {
            // Arrange
            var mockRepo = new Mock<IDataRepository<RoomType, Guid>>();
            mockRepo.Setup(repo => repo.GetAll()).Returns(Task.FromResult(Helper.TestRoomTypeModelList()));
            var controller = new RoomTypeController(mockRepo.Object);

            var result = await controller.GetRoomTypes();

            // Assert
            var viewRoomBooking = Assert.IsType<List<RoomType>>(result);
            var model = Assert.IsAssignableFrom<List<RoomType>>(
               viewRoomBooking);
            Assert.Equal(2, model.Count);
        }

        [Fact]
        public async Task Create_ReturnsBadRequest_GivenInvalidModel()
        {
            // Arrange & Act
            var mockRepo = new Mock<IDataRepository<RoomType, Guid>>();
            var controller = new RoomTypeController(mockRepo.Object);
            controller.ModelState.AddModelError("error", "some error");

            // Act
            var result = await controller.Create(roomType: null);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Create_RoomType()
        {
            // Arrange & Act
            var mockRepo = new Mock<IDataRepository<RoomType, Guid>>();
            var controller = new RoomTypeController(mockRepo.Object);

            // Act
            var result = await controller.Create(Helper.TestRoomType());

            // Assert
            Assert.IsType<OkObjectResult>(result);
            
        }
        
        [Fact]
        public async Task Update_RoomType()
        {
            // Arrange & Act
            var mockRepo = new Mock<IDataRepository<RoomType, Guid>>();
            var controller = new RoomTypeController(mockRepo.Object);

            // Act
            var result = await controller.Update(Helper.TestRoomType());

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }
        
    }
}
