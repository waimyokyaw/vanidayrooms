﻿using System;
using System.Collections.Generic;
using System.Text;
using Vaniday.API.Controllers;
using Vaniday.API.Models;
using Vaniday.API.Models.Repository;

namespace VanidaySPA.Tests
{
    public static class Helper
    {
        public static RoomBooking TestRoomBooking3Days()
        {
            return new RoomBooking()
            {
                
                BookedFrom = DateTime.Now,
                BookedTo = DateTime.Now.AddDays(2),
                BookingRate = 100
            };
        }

        public static RoomBooking TestRoomBooking1Day()
        {
            return new RoomBooking()
            {
                
                BookedFrom = DateTime.Now,
                BookedTo = DateTime.Now,
                BookingRate = 100
            };
        }

        public static List<RoomBooking> TestRoomBookingsModelList()
        {
            var roomBookings = new List<RoomBooking>
            {
                new RoomBooking()
                {
                
                    BookedFrom = DateTime.Now,
                    BookedTo = DateTime.Now.AddDays(1),
                    BookingRate = 100
                },
                new RoomBooking()
                {
                
                    BookedFrom = DateTime.Now,
                    BookedTo = DateTime.Now.AddDays(1),
                    BookingRate = 100
                }
            };
            return roomBookings;
        }

        public static List<RoomType> TestRoomTypeModelList()
        {
            var roomTypes = new List<RoomType>
            {
                new RoomType()
                {
                    RoomTypeName = "Single",
                    RoomTypeDescription = "Single Room"
                },
                new RoomType()
                {
                    RoomTypeName = "Double",
                    RoomTypeDescription = "Double Room"
                }
            };
            return roomTypes;
        }

        public static RoomType TestRoomType()
        {
            return new RoomType()
            {
                RoomTypeName = "Single",
                RoomTypeDescription = "Single Room"
            };
        }
    }
}
