﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Vaniday.API.Controllers;
using Vaniday.API.Models;
using Vaniday.API.Models.Repository;
using System;

namespace VanidaySPA.Tests
{
    public class RoomBookingControllerTests
    {
        [Fact]
        public async Task TestGetRoomBookingList()
        {
            // Arrange
            var mockRepo = new Mock<IDataRepository<RoomBooking, Guid>>();
            mockRepo.Setup(repo => repo.GetAll()).Returns(Task.FromResult(Helper.TestRoomBookingsModelList()));
            var controller = new RoomBookingController(mockRepo.Object);

            var result = await controller.GetAll();

            // Assert
            var viewRoomBooking = Assert.IsType<List<RoomBooking>>(result);
            var model = Assert.IsAssignableFrom<List<RoomBooking>>(
               viewRoomBooking);
            Assert.Equal(2, model.Count);
        }
        
        [Fact]
        public async Task Create_ReturnsBadRequest_GivenInvalidModel()
        {
            // Arrange & Act
            var mockRepo = new Mock<IDataRepository<RoomBooking, Guid>>();
            var controller = new RoomBookingController(mockRepo.Object);
            controller.ModelState.AddModelError("error", "some error");

            // Act
            var result = await controller.Create(roomBooking: null);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Create_Booking3Days()
        {
            // Arrange & Act
            var mockRepo = new Mock<IDataRepository<RoomBooking, Guid>>();
            var controller = new RoomBookingController(mockRepo.Object);

            // Act
            var result = await controller.Create(Helper.TestRoomBooking3Days());

            // Assert
            var viewRoomBooking = Assert.IsType<List<RoomBooking>>(((OkObjectResult)result).Value);
            var model = Assert.IsAssignableFrom<List<RoomBooking>>(
               viewRoomBooking);
            Assert.Equal(3, model.Count);
        }

        [Fact]
        public async Task Create_Booking1Day()
        {
            // Arrange & Act
            var mockRepo = new Mock<IDataRepository<RoomBooking, Guid>>();
            var controller = new RoomBookingController(mockRepo.Object);

            // Act
            var result = await controller.Create(Helper.TestRoomBooking1Day());

            // Assert
            var viewRoomBooking = Assert.IsType<List<RoomBooking>>(((OkObjectResult)result).Value);
            var model = Assert.IsAssignableFrom<List<RoomBooking>>(
               viewRoomBooking);
            Assert.Equal(1, model.Count);
        }

    }
}
