﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vaniday.API.Models;

namespace Vaniday.API.Helper.Data
{

    public static class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            
            if (context.RoomStatuses.Any())
            {
                Console.WriteLine("Db has been seeded");
                return;   // DB has been seeded
            }
            Console.WriteLine("Importing Room Statuses");
            var roomStatuses = new RoomStatus[]
            {
            new RoomStatus{Name="Available"},
            new RoomStatus{Name="Not Available"}
            };
            foreach (RoomStatus s in roomStatuses)
            {
                context.RoomStatuses.Add(s);
            }
            context.SaveChanges();

            Console.WriteLine("Importing Room Types");
            var roomTypes = new RoomType[]
            {
            new RoomType{RoomTypeName="Single"},
            new RoomType{RoomTypeName="Double"}
            };
            foreach (RoomType s in roomTypes)
            {
                context.RoomTypes.Add(s);
            }
            context.SaveChanges();

            Console.WriteLine("Importing data completed.");
        }
    }
}

