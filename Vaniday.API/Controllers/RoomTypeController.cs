﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vaniday.API.Models;
using Vaniday.API.Models.Repository;
using Vaniday.API.Models.DataManager;

namespace Vaniday.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class RoomTypeController : Controller
    {
        private IDataRepository<RoomType, Guid> _iRepo;

        public RoomTypeController(IDataRepository<RoomType, Guid> repo)
        {
            _iRepo = repo;
        }

        // GET: api/RoomType
        [HttpGet]
        public async Task<IEnumerable<RoomType>> GetRoomTypes()
        {
            return await _iRepo.GetAll();
        }

        // GET: api/RoomType/5
        [HttpGet("api/RoomType/{id}")]
        public async Task<RoomType> Get(Guid id)
        {
            return await _iRepo.Get(id);
        }
        
        // POST: api/RoomType
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]RoomType roomType)
        { 
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RoomType createdRoomType = await _iRepo.Add(roomType);
            return Ok(createdRoomType);
        }

        // PUT: api/RoomType/5
        [HttpPut]
        public async Task<IActionResult> Update([FromBody]RoomType roomType)
        {
            var roomTypeUpdated = await _iRepo.Update(roomType.Id, roomType);
            return Ok(roomTypeUpdated);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var roomTypeDeleted = await _iRepo.Delete(id);
            return Ok(roomTypeDeleted);
        }
    }
}
