﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vaniday.API.Models;
using Vaniday.API.Models.Repository;
using Vaniday.API.Models.DataManager;

namespace Vaniday.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class RoomStatusController : Controller
    {
        private IDataRepository<RoomStatus, Guid> _iRepo;

        public RoomStatusController(IDataRepository<RoomStatus, Guid> repo)
        {
            _iRepo = repo;
        }

        // GET: api/RoomStatus
        [HttpGet]
        public async Task<IEnumerable<RoomStatus>> GetRoomStatuses()
        {
            Console.WriteLine("Retrieveing all room statuses");
            return await _iRepo.GetAll();
        }

        // GET: api/RoomStatus/5
        [HttpGet("api/RoomStatus/{id}")]
        public async Task<RoomStatus> Get(Guid id)
        {
            return await _iRepo.Get(id);
        }
        
        // POST: api/RoomStatus
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]RoomStatus roomStatus)
        { 
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RoomStatus createdRoomStatus = await _iRepo.Add(roomStatus);
            return Ok(createdRoomStatus);
        }

        // PUT: api/RoomStatus/5
        [HttpPut]
        public async Task<IActionResult> Update([FromBody]RoomStatus roomStatus)
        {
            var roomStatusUpdated = await _iRepo.Update(roomStatus.Id, roomStatus);
            return Ok(roomStatusUpdated);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var roomStatusDeleted = await _iRepo.Delete(id);
            return Ok(roomStatusDeleted);
        }
    }
}
