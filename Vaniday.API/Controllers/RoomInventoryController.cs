﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vaniday.API.Models;
using Vaniday.API.Models.Repository;
using Vaniday.API.Models.DataManager;

namespace Vaniday.API.Controllers
{
    [Produces("application/json")]
    [Route("api/RoomInventory")]
    public class RoomInventoryController : Controller
    {
        private IDataRepository<RoomInventory, Guid> _iRepo;

        public RoomInventoryController(IDataRepository<RoomInventory, Guid> repo)
        {
            _iRepo = repo;
        }

        // GET: api/RoomInventory
        [HttpGet]
        public async Task<IEnumerable<RoomInventory>> Get()
        {
            return await _iRepo.GetAll();
        }

        // GET: api/RoomInventory/5
        [HttpGet("api/RoomInventory/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            RoomInventory roomInventory = await _iRepo.Get(id);
            if (roomInventory == null)
            {
                return NotFound(id);
            }
            return Ok(roomInventory);
        }

        // POST: api/roomInventory
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]RoomInventory roomInventory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RoomInventory createdRoomInventory = await _iRepo.Add(roomInventory);
            return Ok(createdRoomInventory);
        }
        
        // PUT: api/RoomBooking/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromBody]RoomInventory roomInventory)
        {
            var updatedInventory = await _iRepo.Update(roomInventory.Id, roomInventory);
            return Ok(updatedInventory);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var deletedInventory = await _iRepo.Delete(id);
            return Ok(deletedInventory);
        }
    }
}
