﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vaniday.API.Models;
using Vaniday.API.Models.Repository;
using Vaniday.API.Models.DataManager;

namespace Vaniday.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class RoomBookingController : Controller
    {
        private IDataRepository<RoomBooking, Guid> _iRepo;

        public RoomBookingController(IDataRepository<RoomBooking, Guid> repo)
        {
            _iRepo = repo;
        }

        // GET: api/RoomBooking
        [HttpGet]
        public async Task<IEnumerable<RoomBooking>> GetAll()
        {
            return await _iRepo.GetAll();
        }

        // GET: api/RoomBooking/5
        [HttpGet("api/RoomBooking/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            RoomBooking roomBooking = await _iRepo.Get(id);
            if (roomBooking == null)
            {
                return NotFound(id);
            }
            return Ok(roomBooking);
        }
        
        // POST: api/RoomBooking
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]RoomBooking roomBooking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<RoomBooking> roomBookings = new List<RoomBooking>();
            List<DateTime> bookingDays = Helper.Util.EachDay(roomBooking.BookedFrom, roomBooking.BookedTo).ToList();
            foreach (DateTime bookingDay in bookingDays)
            {
                var rm = roomBooking;
                roomBooking = new RoomBooking();
                roomBooking.BookingRate = rm.BookingRate;
                roomBooking.RoomStatusId = rm.RoomStatusId;
                roomBooking.RoomTypeId = rm.RoomTypeId;
                roomBooking.BookedFrom = bookingDay;
                roomBooking.BookedTo = bookingDay;
                roomBooking.CreatedDate = DateTime.UtcNow;
                roomBooking.ModifiedDate = DateTime.UtcNow;
                var bookingAdded = await _iRepo.Add(roomBooking);
                roomBookings.Add(bookingAdded);
            }
            return Ok(roomBookings);
        }
        
        // PUT: api/RoomBooking
        [HttpPost]
        public async Task<IActionResult> UpdateRoomRate([FromBody]RoomBooking roomBooking)
        {
           
            var bookingUpdated = await _iRepo.Update(roomBooking.Id, roomBooking);
            
            return Ok(bookingUpdated);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var bookingDeleted = await _iRepo.Delete(id);
            return Ok(bookingDeleted);
        }
    }
}
