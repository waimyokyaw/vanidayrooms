﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Vaniday.API.Models;
using Vaniday.API.Models.DataManager;
using Vaniday.API.Models.Repository;

namespace Vaniday.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(opts => opts.UseSqlServer(Configuration["ConnectionString:VanidayRoomsDB"]));
            services.AddScoped(typeof(IDataRepository<RoomBooking, Guid>), typeof(RoomBookingManager));
            services.AddScoped(typeof(IDataRepository<RoomType, Guid>), typeof(RoomTypeManager));
            services.AddScoped(typeof(IDataRepository<RoomInventory, Guid>), typeof(RoomInventoryManager));
            services.AddScoped(typeof(IDataRepository<RoomStatus, Guid>), typeof(RoomStatusManager));
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
