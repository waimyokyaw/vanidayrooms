﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vaniday.API.Models.Repository;

namespace Vaniday.API.Models.DataManager
{
    public class RoomInventoryManager : IDataRepository<RoomInventory, Guid>
    {
        ApplicationDbContext context;
        public RoomInventoryManager(ApplicationDbContext applicationDbContext)
        {
            context = applicationDbContext;
        }

        public Task<RoomInventory> Get(Guid id)
        {
            return context.RoomInventories.FirstOrDefaultAsync(b => b.Id == id);
        }

        public Task<List<RoomInventory>> GetAll()
        {
            return context.RoomInventories.ToListAsync();
        }

        public async Task<RoomInventory> Add(RoomInventory roomInventory)
        {
            context.RoomInventories.Add(roomInventory);
            await context.SaveChangesAsync();
            return roomInventory;
        }

        public async Task<RoomInventory> Delete(Guid id)
        {
            var roomInventory = context.RoomInventories.FirstOrDefault(b => b.Id == id);
            if (roomInventory != null)
            {
                context.RoomInventories.Remove(roomInventory);
                await context.SaveChangesAsync();
            }
            return roomInventory;
        }

        public async Task<RoomInventory> Update(Guid id, RoomInventory item)
        {
            var roomInventory = context.RoomInventories.Find(id);
            if (roomInventory != null)
            {
                roomInventory.RoomType = item.RoomType;
                roomInventory.RoomRate = item.RoomRate;
                roomInventory.NumberOfVacant = item.NumberOfVacant;
                await context.SaveChangesAsync();
            }
            return roomInventory;
        }
    }
}
