﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vaniday.API.Models.Repository;

namespace Vaniday.API.Models.DataManager
{
    public class RoomTypeManager : IDataRepository<RoomType, Guid>
    {
        ApplicationDbContext context;
        public RoomTypeManager(ApplicationDbContext applicationDbContext)
        {
            context = applicationDbContext;
        }

        public Task<RoomType> Get(Guid id)
        {
            return context.RoomTypes.FirstOrDefaultAsync(b => b.Id == id);
        }

        public Task<List<RoomType>> GetAll()
        {
            return context.RoomTypes.ToListAsync();
        }

        public async Task<RoomType> Add(RoomType roomType)
        {
            context.RoomTypes.Add(roomType);
            await context.SaveChangesAsync();
            return roomType;
        }

        public async Task<RoomType> Delete(Guid id)
        {
            var roomType = context.RoomTypes.FirstOrDefault(b => b.Id == id);
            if (roomType != null)
            {
                context.RoomTypes.Remove(roomType);
                await context.SaveChangesAsync();
            }
            return roomType;
        }

        public async Task<RoomType> Update(Guid id, RoomType item)
        {
            var roomType = context.RoomTypes.Find(id);
            if (roomType != null)
            {
                roomType.RoomTypeName = item.RoomTypeName;
                roomType.RoomTypeDescription = item.RoomTypeDescription;
             
                await context.SaveChangesAsync();
            }
            return roomType;
        }
        
    }
}
