﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vaniday.API.Models.Repository;

namespace Vaniday.API.Models.DataManager
{
    public class RoomStatusManager : IDataRepository<RoomStatus, Guid>
    {
        ApplicationDbContext context;
        public RoomStatusManager(ApplicationDbContext applicationDbContext)
        {
            context = applicationDbContext;
        }

        public Task<RoomStatus> Get(Guid id)
        {
            return context.RoomStatuses.FirstOrDefaultAsync(b => b.Id == id);
        }

        public Task<List<RoomStatus>> GetAll()
        {
            return context.RoomStatuses.ToListAsync();
        }

        public async Task<RoomStatus> Add(RoomStatus roomStatus)
        {
            context.RoomStatuses.Add(roomStatus);
            await context.SaveChangesAsync();
            return roomStatus;
        }

        public async Task<RoomStatus> Delete(Guid id)
        {
            var roomStatus = context.RoomStatuses.FirstOrDefault(b => b.Id == id);
            if (roomStatus != null)
            {
                context.RoomStatuses.Remove(roomStatus);
                await context.SaveChangesAsync();
            }
            return roomStatus;
        }

        public async Task<RoomStatus> Update(Guid id, RoomStatus item)
        {
            var roomStatus = context.RoomStatuses.Find(id);
            if (roomStatus != null)
            {
                roomStatus.Name = item.Name;
                
                await context.SaveChangesAsync();
            }
            return roomStatus;
        }
        
    }
}
