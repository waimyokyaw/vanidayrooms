﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vaniday.API.Models.Repository;

namespace Vaniday.API.Models.DataManager
{
    public class RoomBookingManager : IDataRepository<RoomBooking, Guid>
    {
        ApplicationDbContext context;
        public RoomBookingManager(ApplicationDbContext applicationDbContext)
        {
            context = applicationDbContext;
        }

        public Task<RoomBooking> Get(Guid id)
        {
            return context.RoomBookings.FirstOrDefaultAsync(b => b.Id == id);
        }

        public Task<List<RoomBooking>> GetAll()
        {
            return context.RoomBookings.ToListAsync();
        }

        public async Task<RoomBooking> Add(RoomBooking roomBooking)
        {
            context.RoomBookings.Add(roomBooking);
            await context.SaveChangesAsync();
            return roomBooking;
        }

        public async Task<RoomBooking> Delete(Guid id)
        {
            var roomBooking = context.RoomBookings.FirstOrDefault(b => b.Id == id);
            if (roomBooking != null)
            {
                context.RoomBookings.Remove(roomBooking);
                await context.SaveChangesAsync();
            }
            return roomBooking;
        }

        public async Task<RoomBooking> Update(Guid id, RoomBooking item)
        {
            var roomBooking = context.RoomBookings.Find(id);
            if (roomBooking != null)
            {
                roomBooking.BookingRate = item.BookingRate;
                roomBooking.ModifiedDate = DateTime.UtcNow;

                await context.SaveChangesAsync();
            }
            return roomBooking;
        }
    }
}
