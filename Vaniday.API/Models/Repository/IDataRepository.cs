﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Vaniday.API.Models.Repository
{
    public interface IDataRepository<TEntity, U> where TEntity : class
    {
        Task<List<TEntity>> GetAll();
        Task<TEntity> Get(U id);
        Task<TEntity> Add(TEntity b);
        Task<TEntity> Update(U id, TEntity b);
        Task<TEntity> Delete(U id);
    }
}
