﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Vaniday.API.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }
        
        public DbSet<RoomType> RoomTypes { get; set; }
        public DbSet<RoomBooking> RoomBookings { get; set; }
        public DbSet<RoomInventory> RoomInventories { get; set; }
        public DbSet<RoomStatus> RoomStatuses { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RoomType>().ToTable("RoomTypes");
            modelBuilder.Entity<RoomBooking>().ToTable("RoomBookings");
            modelBuilder.Entity<RoomInventory>().ToTable("RoomInventories");
            modelBuilder.Entity<RoomStatus>().ToTable("RoomStatuses");
        }
    }
}
